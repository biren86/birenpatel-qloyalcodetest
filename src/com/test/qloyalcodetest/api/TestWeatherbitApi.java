package com.test.qloyalcodetest.api;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.test.qloyalcodetest.apiutil.ApiTestBase;
import com.test.qloyalcodetest.conf.Env;

public class TestWeatherbitApi extends ApiTestBase{

	@Parameters({ "latitude", "longitude" })
	@Test(enabled = true)
	public void getForecastByLatitudesAndLongitudes(
			@Optional("38.123") float latitude, 
			@Optional("-78.543") float longitude
			) throws Exception {
		try {
			// validate latitude and longitude
			if(latitude < -90 || latitude > 90 || longitude < -180 || longitude > 180) {
				log.info("Invalid cordinates provided");
				throw new IllegalArgumentException("Invalid cordinates provided");
			}

			// build URL to get forecast by latitude and longitude
			String rawForecastUrl = Env.HTTPS + Env.url + Env.weather_by_lat_and_lon + Env.user_key;
			String forecastUrl = String.format(rawForecastUrl, latitude, longitude);

			// get forecast
			HttpResponse response = execGet(forecastUrl);
			JSONObject jsonResp = getResponse(response);

			JSONArray forecastArray = jsonResp.getJSONArray("data");
			JSONObject forecastObject = forecastArray.getJSONObject(0);

			double latResp = forecastObject.getDouble("lat");
			double lonResp = forecastObject.getDouble("lon");
			int tempResp = forecastObject.getInt("temp");
			int respCountValue = jsonResp.getInt("count");

			assertEquals(response.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
			assertTrue(respCountValue == 1);
			assertNotNull(tempResp);
			assertEquals(latResp, getDoubleValueUptoTwoDecimal(latitude));
			assertEquals(lonResp, getDoubleValueUptoTwoDecimal(longitude));
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	@Parameters({ "postcode"})
	@Test(enabled = true)
	public void getForecastByPostCode(@Optional("K2P 1L4") String postcode) throws Exception {
		try {
			postcode = replaceWhiteSpaceInString(postcode);
			// build URL to get forecast by latitude and longitude
			String rawForecastUrl = Env.HTTPS + Env.url + Env.weather_by_postalCode + Env.user_key;
			String forecastUrl = String.format(rawForecastUrl, postcode);

			// get forecast
			HttpResponse response = execGet(forecastUrl);
			JSONObject jsonResp = getResponse(response);

			JSONArray forecastArray = jsonResp.getJSONArray("data");
			JSONObject forecastObject = forecastArray.getJSONObject(0);

			int tempResp = forecastObject.getInt("temp");
			int respCountValue = jsonResp.getInt("count");

			// the response has no postal code to verify against
			assertEquals(response.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
			assertTrue(respCountValue == 1);
			assertNotNull(tempResp);
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
}
