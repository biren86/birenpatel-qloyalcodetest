package com.test.qloyalcodetest.util;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

public class BasePage {

	public static Logger log = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());
	protected WebDriver driver;

	public BasePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clearAndInput(WebElement webElement, String inputString) {
		waitForLoad(driver);
		webElement.clear();
		webElement.click();
		webElement.sendKeys(inputString);
	}

	public void findAndClick(WebElement webElement) {
		waitForLoad(driver);
		if (webElement.isDisplayed()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
			webElement.click();
		}
	}

	public String getWebElementText(WebElement webElement) {
		return webElement.getText();
	}

	public String getWebElementAttribute(WebElement webElement, String attribute) {
		return webElement.getAttribute(attribute);
	}

	public boolean isDisplayed(WebElement webElement) {

		return webElement.isDisplayed();
	}

	public boolean waitForElement(WebDriver driver, By webElement) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(webElement));

		return true;
	}
	
	public void scrollPageUp() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
	}

	// will wait for page load and javascript code execution
	public void waitForLoad(WebDriver driver) {
		new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
		((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
	}


	public void explicitSleep(int sleep) { 
		try { 
			Thread.sleep(sleep); 
		} catch
		(InterruptedException e) { log.info(e.getLocalizedMessage());
		e.printStackTrace(); Assert.fail(e.getLocalizedMessage() + "\n" +
				e.getStackTrace()); 
		} 
	}

}
