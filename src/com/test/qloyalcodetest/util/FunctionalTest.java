package com.test.qloyalcodetest.util;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.test.qloyalcodetest.conf.Env;

public class FunctionalTest {

	public static Env env = Env.instance();
	public static Logger log = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());
	protected WebDriver driver;

	public void setUp() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(Env.HTTPS + Env.php_travels_url);
	}

	public void tearDown() {
		if (driver != null) {
			try {
				driver.manage().deleteAllCookies();
				driver.quit();
			} catch (Exception e) {
				log.info(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
	}
}
