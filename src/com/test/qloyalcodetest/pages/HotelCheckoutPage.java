package com.test.qloyalcodetest.pages;

import static org.testng.Assert.assertTrue;
import java.util.HashMap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.test.qloyalcodetest.util.BasePage;

public class HotelCheckoutPage extends BasePage {

	@FindBy(xpath = "//div[@id='login']//input[@id='email']")
	WebElement emailField;

	@FindBy(id = "password")
	WebElement passwordField;

	@FindBy(id = "first_name")
	WebElement firstName;

	@FindBy(id = "last_name")
	WebElement lastName;

	@FindBy(xpath = "//div[@class='col-md-6']//input[@id='email']")
	WebElement shopperEmail;

	@FindBy(xpath = "//option[contains(text(),'Australia')]")
	WebElement country;

	@FindBy(id = "phone_number")
	WebElement phoneNumber;

	@FindBy(id = "cardHolderName")
	WebElement cardHolderName;

	@FindBy(id = "cardNumber")
	WebElement cardNumber;

	@FindBy(id = "cardCVC")
	WebElement cardCvc;

	@FindBy(xpath = "//select[@id='month']//option[@value='12']")
	WebElement expiryDay;

	@FindBy(xpath = "//option[@value='2024']")
	WebElement expiryMonth;

	@FindBy(id = "completeBooking")
	WebElement completeBooking;

	@FindBy(xpath = "//div[contains(text(),'Booking generated successfully')]")
	WebElement bookingConfirmationMessage;

	@FindBy(id = "printablediv")
	WebElement bookingInvoicePage;
	
	@FindBy(id = "cookyGotItBtn")
	WebElement userCookiesAcceptButton;

	private  HashMap<WebElement, String> getShopperInfo() {
		HashMap<WebElement, String> userInfo = new HashMap<WebElement, String>();
		userInfo.put(firstName, "TestFN");
		userInfo.put(lastName, "TestLN");
		userInfo.put(shopperEmail, "test@test.com");
		userInfo.put(phoneNumber, "8001352468");
		userInfo.put(cardHolderName, "TestFN TestLN");
		userInfo.put(cardNumber, "4111111111111111");
		userInfo.put(cardCvc, "411");

		return userInfo;
	}

	public HotelCheckoutPage(WebDriver driver) {
		super(driver);
	}

	public void checkout(String username, String password) {
		HashMap<WebElement, String> userInfo = getShopperInfo();
		fillShippingDetails(username, password, userInfo);
		fillPaymentDetails(userInfo);
		findAndClick(completeBooking);
		assertTrue(isDisplayed(bookingConfirmationMessage));
	}

	public boolean isBookingInvoiceDisplayed() {
		return isDisplayed(bookingInvoicePage);
	}

	private void fillPaymentDetails(HashMap<WebElement, String> userInfo) {
		scrollPageUp();
		clearAndInput(cardHolderName, userInfo.get(cardHolderName));
		clearAndInput(cardNumber, userInfo.get(cardNumber));
		clearAndInput(cardCvc, userInfo.get(cardCvc));
		findAndClick(expiryDay);
		findAndClick(expiryMonth);
	}

	private void fillShippingDetails(String username, String password, HashMap<WebElement, String> userInfo) {
		clearAndInput(emailField, username);
		clearAndInput(passwordField, password);
		clearAndInput(firstName, userInfo.get(firstName));
		clearAndInput(lastName, userInfo.get(lastName));
		clearAndInput(shopperEmail, userInfo.get(shopperEmail));
		clearAndInput(phoneNumber, userInfo.get(phoneNumber));
		findAndClick(country);
	}

}
