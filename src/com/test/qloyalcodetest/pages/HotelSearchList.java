package com.test.qloyalcodetest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.test.qloyalcodetest.util.BasePage;

public class HotelSearchList extends BasePage {

	By hotelSearchListing = By.id("listing");

	@FindBy(xpath = "//tr[1]//td[1]//div[3]//form[1]//button[1]")
	WebElement selectFirstHotelOption;

	@FindBy(xpath = "//div[@class='room-cards']//div[1]//div[1]//div[1]//div[1]//div[3]//form[1]//button[1]")
	WebElement bookNowButton;

	public HotelSearchList(WebDriver driver) {
		super(driver);
	}

	public void selectHotelDetails() {
		waitForElement(driver, hotelSearchListing);
		findAndClick(selectFirstHotelOption);
		findAndClick(bookNowButton);
	}
}
