package com.test.qloyalcodetest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.test.qloyalcodetest.util.BasePage;

public class TravelHomePage extends BasePage {

	@FindBy(css = "li.text-center:nth-child(1) > a.text-center")
	WebElement hotelsTab;

	@FindBy(css = "li.text-center:nth-child(2) > a.text-center")
	WebElement flightsTab;

	@FindBy(css = "li.text-center:nth-child(3) > a.text-center")
	WebElement ToursTab;

	@FindBy(id = "s2id_location")
	WebElement hotelSearchBox;

	@FindBy(xpath = "/html[1]/body[1]/div[18]/div[1]/input[1]")
	WebElement searchHotelField;

	@FindBy(css = ".select2-highlighted:nth-child(1) > div.select2-result-label")
	WebElement selectFirstHotelOption;

	@FindBy(css = ".input-lg.hcheckin")
	WebElement checkInDate;

	@FindBy(css = ".input-lg.hcheckout")
	WebElement checkOutDate;

	@FindBy(xpath = "//div[13]//div[1]//tr[1]//th[3]")
	WebElement selectCheckinNextMonth;

	@FindBy(xpath = "//div[13]//tr[4]//td[4]")
	WebElement selectCheckInDate;

	@FindBy(xpath = "//div[14]//div[1]//tr[1]//th[3]")
	WebElement selectCheckoutNextMonth;

	@FindBy(xpath = "//div[14]//tr[4]//td[4]")
	WebElement selectCheckOutDate;

	@FindBy(xpath = "//button[@class='btn btn-lg btn-block btn-primary pfb0 loader']")
	WebElement searchHotelsButton;
	
	@FindBy(id = "cookyGotItBtn")
	WebElement userCookiesAcceptButton;

	public TravelHomePage(WebDriver driver) {
		super(driver);
	}

	public void searchForHotelOrCity(String city) {
		searchHotelOrCityName(city);
		selectDates();
		findAndClick(searchHotelsButton);
		if(isDisplayed(userCookiesAcceptButton)) {
			findAndClick(userCookiesAcceptButton);
		}
	}

	private void selectDates() {
		findAndClick(checkInDate);
		findAndClick(selectCheckinNextMonth);
		findAndClick(selectCheckInDate);
		findAndClick(selectCheckoutNextMonth);
		findAndClick(selectCheckOutDate);
	}

	private void searchHotelOrCityName(String city) {
		findAndClick(hotelsTab);
		findAndClick(hotelSearchBox);
		clearAndInput(searchHotelField, city);
		findAndClick(selectFirstHotelOption);
	}
}
