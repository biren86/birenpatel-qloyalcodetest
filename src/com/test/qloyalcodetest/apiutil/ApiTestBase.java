package com.test.qloyalcodetest.apiutil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.test.qloyalcodetest.conf.Env;

public class ApiTestBase {

	public static Env env = Env.instance();
	public static Logger log = (Logger) LoggerFactory
			.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());
	public ObjectMapper objMap = new ObjectMapper();
	public ObjectWriter pretty = objMap.writerWithDefaultPrettyPrinter();
	public final String HTTPS_OK = "ok";

	public HttpClient createClient() throws Exception {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();

		return httpClient;
	}

	public HttpResponse execGet(String url) throws Exception, IOException, Exception {
		HttpClient client = createClient();
		HttpGet get = new HttpGet(url);
		HttpResponse resp = client.execute(get);
		if (isStatusOK(resp)) {
			return resp;
		}
		throw new Exception("Error making GET call: " + resp.getStatusLine().getStatusCode());
	}

	public boolean isStatusOK(HttpResponse resp) {
		int responseStatusCode = resp.getStatusLine().getStatusCode();
		String responseStat = resp.getStatusLine().getReasonPhrase();

		return (responseStatusCode == HttpStatus.SC_OK && responseStat.equalsIgnoreCase(HTTPS_OK));
	}

	public JSONObject getResponse(HttpResponse response)
			throws UnsupportedOperationException, IOException, org.xml.sax.SAXException {
		HttpEntity entity = response.getEntity();
		InputStream content = entity.getContent();
		InputStreamReader isr = new InputStreamReader(content);
		BufferedReader br = new BufferedReader(isr);
		StringBuffer sb = new StringBuffer();
		String line = "";
		while ((line = br.readLine()) != null) {
			sb.append(line + "\n");
		}
		JSONObject myObject = new JSONObject(sb.toString());

		return myObject;
	}

	public String replaceWhiteSpaceInString(String str) {
		str = str.trim();
		str = str.replaceAll(" ", "%20");

		return str;
	}
	
	public Object getDoubleValueUptoTwoDecimal(double doubleValue) {
		
		return Double.parseDouble(new DecimalFormat("##.##").format(doubleValue));
	}
}
