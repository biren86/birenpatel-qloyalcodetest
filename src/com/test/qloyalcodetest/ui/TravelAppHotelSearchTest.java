package com.test.qloyalcodetest.ui;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.test.qloyalcodetest.conf.Env;
import com.test.qloyalcodetest.pages.HotelCheckoutPage;
import com.test.qloyalcodetest.pages.HotelSearchList;
import com.test.qloyalcodetest.pages.TravelHomePage;
import com.test.qloyalcodetest.util.FunctionalTest;

public class TravelAppHotelSearchTest extends FunctionalTest {

	private TravelHomePage travelHomePage;
	private HotelSearchList hotelSearchList;
	private HotelCheckoutPage hotelCheckoutPage;

	@Override
	@BeforeClass
	public void setUp() {
		super.setUp();
		this.travelHomePage = new TravelHomePage(driver);
		this.hotelSearchList = new HotelSearchList(driver);
		this.hotelCheckoutPage = new HotelCheckoutPage(driver);
	}

	@Override
	@AfterClass
	public void tearDown() {
		super.tearDown();
	}

	@Parameters({ "city"})
	@Test(enabled = true)
	public void testAddProductsToCart(@Optional("Melbourne") String city) {
		travelHomePage.searchForHotelOrCity(city);
		hotelSearchList.selectHotelDetails();
		hotelCheckoutPage.checkout(Env.php_travels_username, Env.php_travels_password);
		assertTrue(hotelCheckoutPage.isBookingInvoiceDisplayed());
	}
}
