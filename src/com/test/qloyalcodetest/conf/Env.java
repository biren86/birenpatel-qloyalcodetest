package com.test.qloyalcodetest.conf;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Env {

	private static final String SRC_RESOURCE = "src/com/test/qloyalcodetest/conf/";
	static Logger log = (Logger) LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());
	private static Env conf = null;
	Properties props = null;

	public static Env instance() {
		if (conf == null) {
			conf = new Env();
			try {
				String testProp = "qloyalcodetest.properties";
				testProp = SRC_RESOURCE + testProp;
				log.info("-DtestProperties=" + testProp);
				String testSuite = "testng";
				log.info("-DtestSuite=" + testSuite);
				conf.props = loadProps(testProp);
				url = getEnv("WEATHERBIT_URL");
				user_key = getEnv("KEY");
				weather_by_lat_and_lon = getEnv("GET_WEATHER_BY_LAT_AND_LON");
				weather_by_postalCode = getEnv("GET_WEATHER_BY_POSTAL_CODE");
				php_travels_url = getEnv("PHP_TRAVELS_URL");
				php_travels_username = getEnv("PHP_TRAVELS_USERNAME");
				php_travels_password = getEnv("PHP_TRAVELS_PASSWORD");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return conf;
	}

	public static String url = null;
	public static String user_key = null;
	public static String weather_by_lat_and_lon = null;
	public static String weather_by_postalCode = null;
	public static String php_travels_url = null;
	public static String php_travels_username = null;
	public static String php_travels_password = null;
	
	public static String HTTPS = "https://";

	public static String getEnv(String key) {
		String val = conf.props.getProperty(key);
		if (val == null || val.length() == 0) {
			return val;
		}
		val = val.trim();
		return val;
	}

	public static String getEnv(String key, String defValue) {
		String val = conf.props.getProperty(key, defValue);
		val.trim();
		if (val == null || val.length() == 0) {
			val = defValue;
		}
		return val;
	}

	public static void printProps(Properties props) {
		Enumeration<Object> keys = props.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = (String) props.get(key);
			log.info(key + "==" + value);
		}
	}

	public static Properties loadProps(String file) throws Exception {
		log.info("loadProps = " + file);
		Properties p = new Properties();
		if (file != null && !file.equals("")) {
			p.load(new FileInputStream(file));
		}
		return p;
	}

	Properties getProps() {
		return conf.props;
	}
}
